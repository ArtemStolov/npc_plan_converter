from argparse import ArgumentParser
from datetime import datetime
from logging import DEBUG, INFO, basicConfig
from os import getcwd
from os.path import join

from src.config import read_config
from src.io import write_column_file
from src.model import Plan, Entities

__all__ = [
    'run_plan_converter',
]


def run_plan_converter():
    parser = ArgumentParser(
        description='Инструмент консольного экспорта данных из ABRA ERP.'
    )
    parser.add_argument('-c', '--config', required=False,
                        default=join(getcwd(), 'config.yml'))
    parser.add_argument('-d', '--debug', required=False, action='store_true',
                        default=False)

    args = parser.parse_args()

    basicConfig(level=args.debug and DEBUG or INFO)

    config = read_config(args.config)

    entities = Entities()
    entities.read_spec_from_column_file(config['input']['spec'])

    plan = Plan(entities)
    plan.read_plan_from_column_file(config['input']['plan'])
    new_plan = plan.copy(datetime.min, datetime.max)
    new_plan.add_wip(config['input']['wip'])
    new_plan.substract_done(config['input']['done'])
    write_column_file(new_plan.column_file_data, config['output']['plan'])


if __name__ == '__main__':
    run_plan_converter()
