from setuptools import find_packages, setup

setup(
    name='npc_plan_converter',
    version='0.1.0a1',
    packages=find_packages(),
    url='',
    license='MIT',
    author='BFG-Soft',
    author_email='saa@bfg.ai',
    description='Script to make new plan from base plan, wip and stock transactions',
    install_requires=[
        'PyYAML',
        'openpyxl>=2.6.0',
    ],
    entry_points={
        'console_scripts': [
            ''
        ]
    }
)
