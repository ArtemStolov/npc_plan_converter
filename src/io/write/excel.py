from datetime import datetime
from operator import itemgetter
from typing import Any, Iterable, Iterator, Mapping, Optional

from openpyxl import Workbook, load_workbook
from openpyxl.styles import Alignment, Border, PatternFill, Side
from openpyxl.worksheet.worksheet import Worksheet

from src.base import Base

__all__ = [
    'write_excel_file',
]

_CELL_SIDE = Side(border_style='thin', color='DDDDDD')
_CELL_BORDER = Border(left=_CELL_SIDE,
                      right=_CELL_SIDE,
                      top=_CELL_SIDE,
                      bottom=_CELL_SIDE)
_CELL_ALIGN = Alignment(vertical='center')
_CELL_BACKGROUND = PatternFill(start_color='EEEEEEEE',
                               end_color='EEEEEEEE',
                               fill_type='solid')

_TITLE_ALIGN = Alignment(horizontal='center')

_PAGE_HEADER_FONT_SIZE = 14


def _as_text(value: Any) -> str:
    if value is None:
        return ''
    return str(value)


class _Workbook(Base):

    def __init__(self,
                 workbook: Workbook,
                 filepath: str,
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._workbook = workbook
        self._filepath = filepath

    @property
    def _worksheet(self) -> Worksheet:
        return self._workbook.active

    def __enter__(self) -> '_Workbook':
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.close()

    def close(self) -> None:
        workbook = self._workbook
        filepath = self._filepath

        self._logger.info('Сохраняем excel файл {!r}.'.format(filepath))
        workbook.save(filepath)
        workbook.close()

    def _write_title(self, title: str, merge_length: int) -> None:
        worksheet = self._worksheet
        worksheet.append([title])
        worksheet.merge_cells(start_row=1,
                              start_column=1,
                              end_row=1,
                              end_column=merge_length)
        current_cell = worksheet['A1']
        current_cell.alignment = _TITLE_ALIGN

    def _format_columns(self, title: Optional[str]) -> None:
        worksheet = self._worksheet

        first_row_number = 1 if title is None else 2

        column_dimensions = worksheet.column_dimensions

        first_row_cells = next(worksheet.iter_rows(
            min_row=first_row_number,
            max_row=first_row_number,
        ))

        # ширину стобцов делаем по максимальной длине текста
        for column_cell, column_values in zip(
            first_row_cells,
            worksheet.iter_cols(
                min_row=first_row_number,
                values_only=True,
            )
        ):
            max_length = max(map(
                lambda value: len(_as_text(value)),
                column_values
            )) + 1

            try:
                column_dimensions[column_cell.column_letter].width = max_length

            except (ValueError, AttributeError):
                column_dimensions[column_cell.column].width = max_length

    def _configure_page_options(self, title: Optional[str]) -> None:
        worksheet = self._worksheet

        page_setup = worksheet.page_setup
        page_setup.orientation = worksheet.ORIENTATION_LANDSCAPE
        page_setup.paperSize = worksheet.PAPERSIZE_A4
        page_setup.fitToHeight = False

        worksheet.sheet_properties.pageSetUpPr.fitToPage = True

        if title is None:
            worksheet.print_title_rows = '1:1'
            # фиксируем первую строку и первый столбец
            worksheet.freeze_panes = worksheet['B2']

        else:
            worksheet.print_title_rows = '1:2'
            worksheet.freeze_panes = worksheet['B3']

        worksheet.print_options.gridLines = True

        odd_header = worksheet.oddHeader
        odd_header_right = odd_header.right
        odd_header_left = odd_header.left

        odd_header_right.text = "{}Page &[Page] of &N".format(
            '{} '.format(title) if title is not None else ''
        )
        odd_header_right.size = _PAGE_HEADER_FONT_SIZE

        odd_header_left.text = datetime.now().isoformat()
        odd_header_left.size = _PAGE_HEADER_FONT_SIZE

    def _format_cells(self) -> None:
        for row in self._worksheet:

            for cell in row:
                cell.border = _CELL_BORDER
                cell.alignment = _CELL_ALIGN

                if cell.row % 2 == 0:
                    cell.fill = _CELL_BACKGROUND

    def write(self,
              data: Iterable[Mapping],
              title: Optional[str] = None,
              format_columns: bool = True,
              format_cells: bool = True,
              format_page: bool = True) -> None:
        worksheet = self._worksheet

        self._logger.info('Выполняется запись листа {!r} excel книги {!r}.'
                          .format(worksheet.title, self._filepath))

        if not isinstance(data, Iterator):
            data = iter(data)

        try:
            headers = next(data)

        except StopIteration:
            self._logger.warning('Нет данных для записи.')
            return

        if title is not None:
            self._write_title(title, len(headers))

        row_keys = tuple(headers)
        row_values_getter = itemgetter(*row_keys)

        # Запись заголовка
        worksheet.append(row_keys)

        # Запись первого ряда
        worksheet.append(row_values_getter(headers))

        for row_values in map(row_values_getter, data):
            worksheet.append(row_values)

        if format_columns:
            self._format_columns(title)

        if format_cells:
            self._format_cells()

        if format_page:
            self._configure_page_options(title)

    @classmethod
    def create_or_load(
        cls,
        worksheet_name: str,
        filepath: str,
        overwrite: Optional[bool] = True,
    ) -> '_Workbook':

        worksheet_name = worksheet_name.replace("/", "")

        if overwrite:
            workbook = Workbook()
            worksheet = workbook.active
            worksheet.title = worksheet_name

        else:
            workbook = load_workbook(filepath)
            worksheet = workbook.create_sheet(worksheet_name)
            workbook.active = worksheet

        worksheet.sheet_properties.tabColor = "1072BA"
        return cls(workbook, filepath)


def write_excel_file(filepath: str,
                     worksheet_name: str,
                     data: Iterable[Mapping],
                     overwrite: bool = True,
                     title: Optional[str] = None,
                     **format_kwargs: bool) -> None:
    with _Workbook.create_or_load(
        worksheet_name,
        filepath,
        overwrite=overwrite,
    ) as workbook:
        workbook.write(data, title=title, **format_kwargs)
