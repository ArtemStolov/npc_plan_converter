from csv import DictWriter
from time import time
from typing import Iterable, Iterator, Mapping

from src.base import Base

__all__ = [
    'CsvWriter',
    'write_csv_file',
]


class CsvWriter(Base):

    def write(self,
              filepath: str,
              data: Iterable[Mapping]) -> None:
        """
        Запись последовательности словарей в csv файл.
        """
        logger = self._logger

        logger.info('Начинаем запись csv файла {!r}.'.format(filepath))
        start_time = time()

        if not isinstance(data, Iterator):
            data = iter(data)

        try:
            headers = next(data)

        except StopIteration:
            self._logger.warning('Нет данных для записи.')
            return

        with open(filepath, 'w', encoding='utf-8') as f:
            writer = DictWriter(f, headers)
            writer.writeheader()
            writer.writerows(data)

        logger.info(
            'Сохранение в csv файл {!r} успешно завершено за '
            '{:.2f} секунд'.format(filepath, time() - start_time)
        )


write_csv_file = CsvWriter().write
