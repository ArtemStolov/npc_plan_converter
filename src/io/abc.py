from abc import ABCMeta, abstractmethod
from os.path import splitext
from typing import Any

from src.base import Base

__all__ = [
    'AbstractColumnFileIOHandler',
]


class AbstractColumnFileIOHandler(Base, metaclass=ABCMeta):
    """Абстрактный обработчик ввода-вывода файлов csv/excel."""

    @abstractmethod
    def _handle_excel(self, filepath: str, *args, **kwargs) -> Any: ...

    @abstractmethod
    def _handle_csv(self, filepath: str, *args, **kwargs) -> Any: ...

    def _handle(self, filepath: str, *args, **kwargs) -> Any:
        logger = self._logger

        ext = splitext(filepath)[1].lower()

        logger.debug('Расширение файла распознано как {}.'.format(ext))

        if ext == '.csv':
            return self._handle_csv(filepath, *args, **kwargs)

        if ext == '.xlsx':
            return self._handle_excel(filepath, *args, **kwargs)

        logger.error('Файлы с расширением {!r} не поддерживаются.'
                     .format(ext))
        raise OSError()
