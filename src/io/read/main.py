from typing import Dict, Mapping, Optional, Tuple, Union

from .csv import CsvReader
from .excel import ExcelReader
from ..abc import AbstractColumnFileIOHandler

__all__ = [
    'ColumnFileReader',
    'read_column_file',
]


class ColumnFileReader(AbstractColumnFileIOHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._excel_reader = ExcelReader()
        self._csv_reader = CsvReader()

    def _handle_excel(self,
                      filepath: str,
                      *args,
                      worksheet_identity: Optional[Union[str, int]] = None,
                      columns_map: Optional[Mapping[str, str]] = None,
                      **kwargs) -> Tuple[Dict, ...]:
        return self._excel_reader.read_one(
            filepath,
            worksheet_identity=worksheet_identity,
            columns_map=columns_map,
            title_row_number=kwargs.get('title_row_number') or 1
        )

    def _handle_csv(self,
                    filepath: str,
                    *args,
                    columns_map: Optional[Mapping[str, str]] = None,
                    **kwargs) -> Tuple[Dict, ...]:
        return self._csv_reader.read(
            filepath,
            columns_map=columns_map
        )

    def read(self, config: dict) -> Tuple[Dict, ...]:
        return self._handle(
            config['file'],
            worksheet_identity=config.get('worksheet'),
            columns_map=config.get('columns'),
            title_row_number=config.get('title_row_number') or 1
        )


read_column_file = ColumnFileReader().read
