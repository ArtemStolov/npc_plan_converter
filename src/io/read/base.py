from operator import itemgetter
from typing import Dict, Iterator, Mapping, Optional, Tuple

from src.base import Base

__all__ = [
    'BaseColumnFileReader',
]


def _is_empty(value):
    return value is None or value == ''


class BaseColumnFileReader(Base):

    def _iter_rows(self,
                   values: Iterator[Tuple],
                   columns_map: Optional[Mapping[str, str]],
                   start_row_number: int = 2,
                   row_number_key: Optional[str] = None) -> Iterator[Dict]:
        logger = self._logger

        try:
            title_row_values = next(values)

        except StopIteration:
            logger.warning('Не найдена строка заголовков.')
            return

        if columns_map is None:
            columns_map = {}

        else:
            # Карта наименований колонок содержит структуру:
            #   'требуемая колонка' - 'колонка в файле'
            # Нам же нужна обратная карта:
            #   'колонка в файле' - 'требуемая колонка'
            # Потому, меняем ключи со значениями в карте местами.
            columns_map = {v: k for k, v in columns_map.items()}

        column_names = []
        column_indices = []

        for index, column_name in enumerate(title_row_values):
            if _is_empty(column_name):
                continue
            if column_name not in columns_map:
                continue

            column_names.append(
                # Если колонка есть в карте преобразований - считываем её
                # оттуда, иначе оставляем оригинальное имя, как в файле.
                columns_map.get(column_name, column_name)
            )
            column_indices.append(index)

        if not column_names:
            logger.error('В строке заголовков не найдено ни одной колонки.')
            return

        set_row_number_key = row_number_key is not None

        for row_number, row_values in enumerate(
            map(itemgetter(*column_indices), values),
            start=start_row_number,
        ):
            if all(map(_is_empty, row_values)):
                continue

            result = dict(zip(column_names, row_values))

            if set_row_number_key:
                result[row_number_key] = row_number

            yield result
