import math
from collections import defaultdict
from datetime import datetime, timedelta
from typing import Dict, Iterable, Iterator, Union

from src.base import Base
from src.io import read_column_file
from .factory import Factory
from .store import Store

__all__ = [
    'PlanEntry',
    'Plan',
]


class PlanEntry(object):  # строка плана
    def __init__(self, product, amount, order_name,
                 entry_date=datetime.now()):
        self.product = product
        self.amount = amount
        self.date = entry_date
        self.order_name = order_name
        self.initial_amount = amount

    def __repr__(self):
        return self.product.description + 'Количество:' + str(
            self.amount) + ' Дата:' + str(self.date) + '\n'

    @property
    def description(self):
        return self.product.description + 'Количество:' + str(
            self.amount) + ' \nДата:' + str(self.date) + '\n \n'


class Plan(Base, Iterable):  # план

    def __init__(self, entities, name='', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.plan_entry = []
        self.entities = entities

    def __repr__(self):
        report = 'План:' + self.name + '\n'
        for entry in self.plan_entry:
            report += entry.description
        return report

    def __iter__(self) -> Iterator[PlanEntry]:
        return iter(self.plan_entry)

    def concatenate_with(self, other):
        for entry in other.plan_entry:
            self.plan_entry.append(entry)

    @property
    def start_date(self):
        result = datetime.max
        for entry in self.plan_entry:
            result = min(result, entry.date)
        return result

    @property
    def finish_date(self):
        result = datetime.min
        for entry in self.plan_entry:
            result = max(result, entry.date)
        return result

    @property
    def number_of_entries(self):  # количество строк в плане
        return len(self.plan_entry)

    @property
    def total_amount(self):  # количество штук/метров в плане
        total_amount = 0
        for entry in self.plan_entry:
            total_amount += entry.amount
        return total_amount

    @property
    def product_set(self):  # множество уникальных продуктов в плане
        # product_set = set()
        # for entry in self.plan_entry:
        #     product_set.add(entry.product)
        # return product_set
        return set(self.plan_entry)

    @property
    def sorted_product_set(
            self):
        # множество уникальных продуктов в плане, сортированных по трудоемкости
        return sorted(self.product_set,
                      key=lambda product: product.labor_per_plan(self),
                      reverse=True)

    def add(self, product_id, amount=0.0, date=datetime.now(),
            code=None, name=None, order_name=None):  # добавить строку в план
        product = self.entities.get_product_with_id(product_id)
        logger = self._logger
        if amount == 0:
            logger.warning(
                'Нулевое количество по продукту {}'
                .format(product_id)
            )
            return

        if product is None:

            if code is None and name is None:
                logger.warning('Поля IDENTITY и NAME отсутствуют '
                               'для продукта {}.'.format(product_id))
                return

            logger.warning(
                'Не найден продукт {}, пробуем определять '
                'по поляем IDENTITY и NAME'.format(product_id)
            )

            product = self.entities.get_product_with_code_name(code, name)
            if product is None:
                logger.warning(
                    'Не найден продукт {} {}, пробуем определить '
                    'только по IDENTITY'.format(code, name)
                )

                if code is None:
                    logger.warning('Поле IDENTITY отсутствует '
                                   'для продукта {}.'.format(product_id))
                    return

                product = self.entities.get_product_with_code(code)
                if product is None:
                    logger.warning('Не найден продукт {}'.format(code))
                    return
                else:
                    logger.warning('Найден продукт {}'.format(product.id))

            else:
                logger.warning('Найден продукт {}'.format(product.id))

        self.plan_entry.append(
            PlanEntry(product=product, amount=amount, entry_date=date,
                      order_name=order_name)
        )

    def clean(self):  # удалить все строки из плана
        self.plan_entry.clear()

    def remove(self, entry):
        self.plan_entry.remove(entry)

    def copy(self, from_date, to_date):
        """
        Копирует строки плана в другой план, с ограничением
        по дате запуска строк плана
        :param from_date: от этой даты
        :type from_date: datetime
        :param to_date: и до этой даты
        :type to_date: datetime
        :return: возвращает план
        """
        temp_copy = Plan(self.entities)
        for each in self.plan_entry:
            if from_date <= each.date < to_date:
                temp_copy.add(
                    product_id=each.product.id,
                    amount=each.amount,
                    date=each.date,
                    order_name=each.order_name,
                    code=each.product.code,
                    name=each.product.name
                )
        return temp_copy

    def workload(self, work_center):
        workload = 0
        for entry in self:
            workload += entry.workload(work_center)
        return workload

    def amount_of_products(self, product, date_from=datetime.min,
                           date_to=datetime.max):
        count = 0
        for entry in self:
            if (entry.product == product) and (
                    date_from < entry.date <= date_to):
                count += entry.amount
        return count

    def labor_per_product(self, product):
        labor = 0
        for entry in self:
            if entry.product == product:
                labor += entry.product.labor_per_part * entry.amount
        return labor

    def get_tech_proc_of_product_with_id(self, product_id):
        return self.get_product_with_id(product_id).tech_proc

    def get_product_with_id(self, product_id):
        for entry in self:
            if entry.product.id == product_id:
                return entry.product
        return None

    def read_plan_from_column_file(self,
                                   config):
        # считываем из xlsx файла план

        logger = self._logger
        data = read_column_file(config)

        for row in data:
            if row['ROW_NUMBER'] == 1:
                continue
            order_name = '{}_{}'.format('NOT_STARTED', row['ORDER'])
            product_id = row['CODE']
            product_name = row.get('NAME')
            product_code = row.get('IDENTITY')

            try:
                if type(row['AMOUNT']) is str:
                    entry_amount = float(row['AMOUNT'].replace(',', '.'))
                else:
                    entry_amount = float(row['AMOUNT'])
            except(ValueError, AttributeError):
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение ячейки {}'
                    .format(row['ROW_NUMBER'], row['AMOUNT'])
                )
                continue

            try:
                entry_date = datetime.combine(datetime.date(row['DATE']),
                                              datetime.time(row['DATE']))
            except(TypeError, ValueError, AttributeError):
                entry_date = datetime.now()
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'DATE в строке {}, значение ячейки {}'
                    .format(row['ROW_NUMBER'], row['DATE'])
                )
            if entry_amount > 0:
                self.add(product_id=product_id, amount=entry_amount,
                         date=entry_date,
                         code=product_code, name=product_name,
                         order_name=order_name)

            else:
                logger.warning(
                    'Изделие {} из заказа {} в плане с '
                    'нулевым количеством. При расчете учтено не будет'
                    .format(product_id, order_name)
                )

    def add_wip(self, config):
        # считываем НЗП и добавляем в План.

        logger = self._logger
        data = read_column_file(config)

        for row in data:
            if row['ROW_NUMBER'] == 1:
                continue
            order_name = row['ORDER']
            product = self.entities.get_product_with_id(row['CODE'])
            if product is None:
                logger.warning(
                    'Изделие {} из заказа {} не найдено. '
                    'При расчете учтено не будет'
                    .format(row['CODE'], order_name)
                )
                continue

            try:
                if type(row['AMOUNT']) is str:
                    entry_amount = float(row['AMOUNT'].replace(',', '.'))
                else:
                    entry_amount = float(row['AMOUNT'])
            except(ValueError, AttributeError):
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение ячейки {}'
                    .format(row['ROW_NUMBER'], row['AMOUNT'])
                )
                continue

            if entry_amount > 0:
                self.add(product_id=product.id, amount=entry_amount,
                         date=datetime.now(),
                         code=product.code, name=product.name,
                         order_name=order_name)
            else:
                logger.warning(
                    'Изделие {} из заказа {} в плане с '
                    'нулевым количеством. При расчете учтено не будет'
                    .format(product.id, order_name)
                )

            self.substract_product(product, entry_amount)

    def substract_product(self, product, amount):
        logger = self._logger
        amount_left = amount
        for row in self:
            if amount_left == 0:
                return
            if 'NOT_STARTED' not in row.order_name:
                continue
            if product.id == row.product.id:
                amount_to_substract = - min(amount, row.amount, amount_left)
                row.amount += amount_to_substract
                amount_left += amount_to_substract
        amount = amount_left
        for row in self:
            if amount_left == 0:
                return
            if 'NOT_STARTED' not in row.order_name:
                continue
            if product.code == row.product.code:
                amount_to_substract = - min(
                    math.ceil(amount * self.initial_percent_with_entry(
                        row,
                        'NOT_STARTED'
                    )),
                    row.amount,
                    amount_left
                )
                row.amount += amount_to_substract
                amount_left += amount_to_substract
        amount = amount_left
        for row in self:
            if amount_left == 0:
                return
            if 'NOT_STARTED' not in row.order_name:
                continue
            if product.code == row.product.code:
                amount_to_substract = - min(
                    math.ceil(amount * self.current_percent_with_entry(
                        row,
                        'NOT_STARTED'
                    )),
                    row.amount,
                    amount_left
                )
                row.amount += amount_to_substract
                amount_left += amount_to_substract

        logger.warning(
            'Не распределилось {} изделия {}'.format(amount, product.id)
        )

    def substract_done(self, config):
        # считываем НЗП и добавляем в План.

        logger = self._logger
        data = read_column_file(config)

        for row in data:
            if row['ROW_NUMBER'] == 1:
                continue
            code = row['IDENTITY']
            try:
                if type(row['AMOUNT']) is str:
                    entry_amount = float(row['AMOUNT'].replace(',', '.'))
                else:
                    entry_amount = float(row['AMOUNT'])
            except(ValueError, AttributeError):
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение ячейки {}'
                    .format(row['ROW_NUMBER'], row['AMOUNT'])
                )
                continue

            if entry_amount <= 0:
                logger.warning(
                    'Изделие {} отчете с '
                    'нулевым количеством. При расчете учтено не будет'
                    .format(code)
                )

            self.substract_product_with_code(code, entry_amount)

    def substract_product_with_code(self, code, amount):
        logger = self._logger
        amount_left = amount
        for row in self:
            if amount_left == 0:
                return
            if 'NOT_STARTED' not in row.order_name:
                continue
            if code == row.product.code:
                amount_to_substract = - min(
                    math.ceil(amount * self.initial_percent_with_entry(
                        row,
                        'NOT_STARTED'
                    )),
                    row.amount,
                    amount_left
                )
                row.amount += amount_to_substract
                amount_left += amount_to_substract
        amount = amount_left
        for row in self:
            if amount_left == 0:
                return
            if 'NOT_STARTED' not in row.order_name:
                continue
            if code == row.product.code:
                amount_to_substract = - min(
                    math.ceil(amount * self.current_percent_with_entry(
                        row,
                        'NOT_STARTED'
                    )),
                    row.amount,
                    amount_left
                )
                row.amount += amount_to_substract
                amount_left += amount_to_substract

        logger.warning(
            'Не распределилось {} изделия {}'.format(amount, code)
        )

    def initial_percent_with_entry(self, entry, pattern):
        total_amount = 0
        for row in self:
            if pattern in row.order_name and \
                    row.product.code == entry.product.code:
                total_amount += row.initial_amount
        if total_amount == 0:
            return 1
        return entry.initial_amount / total_amount

    def current_percent_with_entry(self, entry, pattern):
        total_amount = 0
        for row in self:
            if pattern in row.order_name and \
                    row.product.code == entry.product.code:
                total_amount += row.amount
        if total_amount == 0:
            return 1
        return entry.amount / total_amount


    @property
    def column_file_data(self) -> Iterator[Dict[str, Union[str, int, float]]]:
        for index, entry in enumerate(self, start=1):
            yield {
                'INITIAL_AMOUNT': entry.initial_amount or 0,
                'AMOUNT': entry.amount,
                'DATE': entry.date,
                'ORDER': entry.order_name,
                'IDENTITY': entry.product.code,
                'NAME': entry.product.name,
                'CODE': entry.product.id
            }
