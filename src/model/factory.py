from src.base import Base
from src.io import read_column_file
from .workcenter import WorkCenter

__all__ = [
    'Factory',
]


class Factory(Base):
    # завод, который состоит из большого количества рабочих центров

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.workcenter = []
        self.wc_sorted_list = {}

    def __iter__(self):
        return iter(self.workcenter)

    def __repr__(self):
        report = ''
        for each_workcenter in self:
            report += each_workcenter.id
        return report

    def add(self, wc_id, amount, utilization_factor, name=None):
        if name is None:
            name = wc_id
        if not self.get_workcenter_with_id(wc_id):
            self.workcenter.append(
                WorkCenter(wc_id=str(wc_id), amount=amount, name=str(name),
                           utilization_factor=utilization_factor))
        else:
            self.get_workcenter_with_id(wc_id).amount += amount

    def get_workcenter_with_id(self, wc_id):
        for each_work_center in self:
            if each_work_center.id == wc_id:
                return each_work_center

    def workcenter_sorted_list(self, plan):
        if plan in self.wc_sorted_list:
            return self.wc_sorted_list[plan]
        from operator import itemgetter
        wc_workload = {}
        for each_work_center in self:
            wc_workload[each_work_center] = each_work_center.workload(plan) / \
                                            each_work_center.amount / \
                                            each_work_center.utilization_factor
        wc_workload = sorted(wc_workload.items(), key=itemgetter(1),
                             reverse=True)
        wc_sorted_list = []
        for i in wc_workload:
            wc_sorted_list.append(i[0])
        self.wc_sorted_list[plan] = wc_sorted_list
        return wc_sorted_list

    def read_from_column_file(self, config):
        logger = self._logger
        data = read_column_file(config)

        for row in data:
            if row['ROW_NUMBER'] == 1:
                continue
            equipment_id = str(row['EQUIPMENT_ID']) + '_' + str(row['DEPT_ID'])
            try:
                amount = int(row['AMOUNT'])
            except(ValueError, AttributeError):
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение в ячейке \'{}\''
                    .format(
                        row['ROW_NUMBER'],
                        row['AMOUNT']
                    )
                )
                continue
            try:
                if type(row['UTILIZATION']) is str:
                    utilization_factor = float(
                        row['UTILIZATION'].replace(',', '.'))
                else:
                    utilization_factor = float(row['UTILIZATION'])
            except(ValueError, AttributeError):
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение в ячейке \'{}\''
                    .format(
                        row['ROW_NUMBER'],
                        row['UTILIZATION']
                    )
                )
                continue
            name = row['NAME']

            self.add(wc_id=equipment_id, amount=amount, name=name,
                     utilization_factor=utilization_factor)
