from src.base import Base
from src.io import read_column_file
from .product import Product

__all__ = [
    'Entities',
]


class Entities(Base):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rows = {}

    def __iter__(self):
        return iter(self.rows)

    def __getitem__(self, key: str) -> 'Product':
        return self.rows[key]

    def get_product_with_id(self, product_id):
        try:
            return self.rows[str(product_id)]
        except KeyError:
            return None

    def get_product_with_code_name(self, code, name):
        if (code is None) and (name is None):
            return None
        for entity in self.rows:
            if (str(self.rows[entity].code) == str(code)) and (
                    str(self.rows[entity].name) == str(name)):
                return self.rows[entity]
        return None

    def get_product_with_code(self, code):
        if code is None:
            return None
        for entity in self.rows:
            if str(self.rows[entity].code) == str(code):
                return self.rows[entity]
        return None

    def add(self, product_id, product_code, product_name, batch_size=0):
        if product_id is None:
            return
        if product_name is None:
            product_name = ''
        if product_code is None:
            product_code = ''
        if self.get_product_with_id(product_id) is None:
            self.rows[str(product_id)] = Product(
                str(product_id),
                str(product_code),
                str(product_name),
                batch_size
            )

    def read_batch_size_from_column_file(self, config):

        logger = self._logger
        data = read_column_file(config)

        for row in data:
            product_id = row['CODE']
            try:
                if type(row['BATCH_SIZE']) is str:
                    batch_size = float(row['BATCH_SIZE'].replace(',', '.'))
                else:
                    batch_size = float(row['BATCH_SIZE'])
            except(ValueError, AttributeError):
                batch_size = 0
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'BATCH SIZE в строке {}, значение в ячейке \'{}\''
                    .format(row['ROW_NUMBER'], row['BATCH_SIZE'])
                )
            if self.get_product_with_id(product_id) is None:
                pass
            else:
                self.get_product_with_id(product_id).batch_size = batch_size

    def read_spec_from_column_file(self, config):

        logger = self._logger
        data = read_column_file(config)

        for row in data:
            if row['ROW_NUMBER'] == 1:
                continue
            parent_id = row['PARENT_CODE']
            parent_code = row['PARENT_IDENTITY']
            parent_name = row['PARENT_NAME']
            child_id = row['CODE']
            child_code = row['IDENTITY']
            child_name = row['NAME']
            try:
                if type(row['AMOUNT']) is str:
                    amount = float(row['AMOUNT'].replace(',', '.'))
                else:
                    amount = float(row['AMOUNT'])
            except(ValueError, AttributeError):
                amount = 0
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение в ячейке \'{}\''
                    .format(row['ROW_NUMBER'], row['AMOUNT']
                            )
                )

            self.add(parent_id, parent_code, parent_name)
            self.add(child_id, child_code, child_name)

            parent = self.get_product_with_id(parent_id)
            child = self.get_product_with_id(child_id)

            if (parent is not None) and (child is not None):
                parent.add_child(child, amount)

    def recalculate_entities_workload(self):
        for product_id in self.rows:
            self.get_product_with_id(product_id).recalculate_workload()

    def update_bom(self):
        logger = self._logger

        logger.info(
            'Расчет потребности в покупных материалах'
        )
        for product in self.rows.values():
            product.bom = product.get_bom(amount=1)
