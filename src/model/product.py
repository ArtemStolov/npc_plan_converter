from collections import defaultdict

from src.base import Base

__all__ = [
    'ProductEntry',
    'Product',
    'Operation',
    'TechProcess',
]


class ProductEntry(object):
    def __init__(self, product, amount):
        self.product = product
        self.amount = amount


class Product(Base):  # изделие
    def __init__(self, prod_id, code, name, batch_size=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = prod_id
        self.code = code
        self.name = name
        self.batch_size = batch_size
        self.tech_proc = TechProcess()
        self.product_entry = []
        self.whole_workload = {}
        self.whole_operation_workload = {}
        self.whole_preparing_workload = {}
        self.bom = {}

    def __repr__(self):
        return self.description

    def __iter__(self):
        return iter(self.tech_proc.operations)

    @property
    def is_raw_material(self):
        if self.tech_proc.operations or self.product_entry:
            return False
        else:
            return True

    @property
    def is_simplest_part(self):
        for each in self.product_entry:
            if each.product.is_raw_material:
                pass
            else:
                return False
        return True

    @property
    def description(self):
        return 'ИД:' + self.id + ' Шифр:' + self.code + ' Имя:' + self.name

    @property
    def max_t_sht(self):
        result = 0
        for each_operation in self.tech_proc.operations:
            result = max(result, each_operation.t_sht)
        return result

    def calculate_batch_size(self, default_batch_operation_hours):
        logger = self._logger
        if self.batch_size == 0:
            batch_size = max(round(
                default_batch_operation_hours / max(self.max_t_sht, 0.0001)),
                1)
            for child in self.product_entry:
                batch_size = min(
                    batch_size,
                    round(
                        child.product.calculate_batch_size(
                            default_batch_operation_hours
                        ) / child.amount
                    )
                )
                batch_size = max(batch_size, 1)
            self.batch_size = batch_size
        return self.batch_size

    def specification_report(self, _i=0):
        report = '\n' + '-' * _i
        if _i == 0:
            report += self.description + '\n' + '-'
        _i += 1

        for child in self.product_entry:
            report += str(child.product.description) + ' Количество:' + str(
                child.amount) \
                      + 'шт' + child.product.specification_report(_i)

        _i -= 1
        return report

    @property
    def labor_per_part(self):  # трудоемкость детали
        labor = 0
        for operation in self:
            labor += operation.operation_hours(self.batch_size)
        for child in self.product_entry:
            labor += child.product.labor_per_part * child.amount
        return labor

    def add_child(self, child, amount):
        if child is None:
            return
        if amount == 0:
            return
        self.product_entry.append(ProductEntry(child, amount))

    def is_product_in_spec(self, child):
        for spec_row in self.product_entry:
            if spec_row.product == child:
                return True
        return False

    def recalculate_workload(self):
        self.whole_workload = self.__whole_workload()
        self.whole_operation_workload = self.__whole_operation_workload()
        self.whole_preparing_workload = self.__whole_preparing_workload()

    def __whole_workload(self,
                         _report=None):
        # загрузка выбранного рабочего центра по продукту (ПЗ + ШТ)
        if _report is None:
            _report = defaultdict(float)
        for operation in self.tech_proc:
            _report[operation.work_center.id] += operation.operation_hours(
                self.batch_size)
        for child in self.product_entry:
            child.product.__whole_workload(_report)
        return _report

    def __whole_operation_workload(self,
                                   _report=None):
        # загрузка выбранного рабочего центра по продукту (ШТ времена)
        if _report is None:
            _report = defaultdict(float)
        for operation in self.tech_proc:
            _report[operation.work_center.id] += operation.t_sht
        for child in self.product_entry:
            child.product.__whole_operation_workload(_report)
        return _report

    def __whole_preparing_workload(self, _report=None,
                                   amount=1):
        # загрузка выбранного рабочего центра по продукту (ПЗ времена)
        if _report is None:
            _report = defaultdict(float)
        for operation in self.tech_proc:
            _report[operation.work_center.id] += operation.t_pz / amount
        for child in self.product_entry:
            child.product.__whole_preparing_workload(
                _report,
                amount=amount * child.amount
            )
        return _report

    def labor_per_plan(self, plan):  # трудоемкость продукта в плане
        return plan.labor_per_product(self)

    def simplify_specification_dict(self, amount, _report=None):
        """
        Пробую сделать функцию, где на выходе словарь,
        состоящий только из деталей низшего уровня изделия
        :param amount: количество деталей в спецификации
        :param _report:
        :return:
        """
        if _report is None:
            _report = defaultdict(float)

        if self.is_raw_material:
            return _report

        if self.is_simplest_part:
            _report[self] += amount
            return _report

        for spec_row in self.product_entry:
            _report.update(spec_row.product.simplify_specification_dict(
                amount * spec_row.amount, _report))

        return _report

    def get_bom(self, amount, _report=None):
        """
        Пробую сделать функцию, где на выходе словарь,
        состоящий только из покупных
        :param amount: количество деталей в спецификации
        :param _report:
        :return:
        """
        if _report is None:
            _report = defaultdict(float)

        if self.is_raw_material:
            _report[self] += amount
            return _report

        for spec_row in self.product_entry:
            _report.update(
                spec_row.product.get_bom(
                    amount * spec_row.amount, _report
                )
            )

        return _report


class Operation(object):  # операция
    def __init__(self, work_center, t_sht, t_pz):
        self.work_center = work_center
        self.t_sht = t_sht
        self.t_pz = t_pz

    def operation_hours(self, batch_size):
        if batch_size == 0:
            return 0
        return self.t_sht + self.t_pz / batch_size


class TechProcess(object):  # техпроцесс
    def __init__(self):
        self.operations = []

    def __repr__(self):
        report = ''
        for operation in self:
            report += str(operation.work_center.id) + ' Тпз:' + str(
                operation.t_pz) + 'ч. Тшт' \
                      + str(operation.t_sht) + '\n'
        return report

    def __iter__(self):
        return iter(self.operations)

    def workload(self, work_center,
                 batch_size):
        # загрузка выбранного рабочего центра по техпроцессу
        workload = 0
        for operation in self:
            if operation.work_center == work_center:
                workload += operation.operation_hours(batch_size)
        return workload

    def add_operation(self, work_center, t_pz,
                      t_sht):  # добавляем операцию в технологический процесс
        self.operations.append(Operation(work_center, t_pz=t_pz, t_sht=t_sht))

