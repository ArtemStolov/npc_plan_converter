from collections import defaultdict
from datetime import datetime, timedelta
from itertools import chain
from operator import attrgetter
from typing import Union

from src.base import Base
from src.io import read_column_file
from .entities import Entities
from .product import Product

__all__ = [
    'StoreTransaction',
    'Store',
]


_ONE_DAY_TIMEDELTA = timedelta(1)


def _round_up_to_day(value: datetime) -> datetime:
    return value.replace(
        hour=0,
        minute=0,
        second=0,
        microsecond=0,
    )


def get_next_date(current_date: datetime,
                  weekday=None, monthday=None) -> datetime:
    cur_date = current_date
    while True:
        cur_date += _ONE_DAY_TIMEDELTA
        if not weekday and not monthday:
            return cur_date
        if cur_date.isoweekday() == weekday:
            return cur_date
        if cur_date.day == monthday:
            return cur_date


def dates_list(from_date, to_date, weekday=None, monthday=None) -> []:
    result = []
    current_date = from_date
    result.append(current_date)
    while current_date <= to_date:
        current_date = get_next_date(current_date, weekday, monthday)
        result.append(current_date)

    return result


class StoreTransaction(Base):

    def __init__(self,
                 date: datetime,
                 quantity: Union[int, float],
                 comments=dict(),
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.date = date
        self.quantity = quantity
        self.comments = comments

    @property
    def get_product(self):
        return self.comments.get('PRODUCT')

    @property
    def get_from(self):
        return self.comments.get('FROM')


class Store(Base):

    def __init__(self,
                 materials=None,
                 transactions=None,
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if materials:
            self.materials = materials
        else:
            self.materials = {}

        self.transactions = defaultdict(list)
        if transactions:
            for material, transaction_list in transactions.items():
                self.transactions[material] = transaction_list[:]

    def _get_material_balance(self,
                              material: Product,
                              date: datetime) -> float:
        return sum(
            map(
                attrgetter('quantity'),
                filter(
                    lambda transaction: transaction.date < date,
                    self.transactions[material]
                )
            )
        )

    def _get_material_cumulative_arrival(self,
                                         material: Product,
                                         date: datetime) -> float:
        return sum(
            map(
                attrgetter('quantity'),
                filter(
                    lambda transaction: transaction.date < date
                                        and transaction.quantity > 0,
                    self.transactions[material]
                )
            )
        )

    def _get_material_cumulative_usage(self,
                                       material: Product,
                                       date: datetime) -> float:
        return sum(
            map(
                attrgetter('quantity'),
                filter(
                    lambda transaction: transaction.date < date
                                        and transaction.quantity < 0,
                    self.transactions[material]
                )
            )
        )

    def _get_material_cumulative_usage_per_product(self,
                                                   material: Product,
                                                   date: datetime,
                                                   product: str) -> float:
        return sum(
            map(
                attrgetter('quantity'),
                filter(
                    lambda transaction: transaction.date < date and
                                        transaction.quantity < 0 and
                                        transaction.get_product == product,
                    self.transactions[material]
                )
            )
        )

    def _get_material_cumulative_usage_per_from(self,
                                                   material: Product,
                                                   date: datetime,
                                                   _from: str) -> float:
        return sum(
            map(
                attrgetter('quantity'),
                filter(
                    lambda transaction: transaction.date < date and
                                        transaction.quantity < 0 and
                                        transaction.get_from == _from,
                    self.transactions[material]
                )
            )
        )

    def _get_material_cumulative_arrival_per_from(self,
                                                  material: Product,
                                                  date: datetime,
                                                  _from: str) -> float:
        return sum(
            map(
                attrgetter('quantity'),
                filter(
                    lambda transaction: transaction.date < date and
                                        transaction.quantity > 0 and
                                        transaction.get_from == _from,
                    self.transactions[material]
                )
            )
        )

    @property
    def _get_all_products_set(self):
        return filter(
            lambda i: i is not None,
            set(
                transaction.get_product
                for material in self.transactions
                for transaction in self.transactions[material]
            )
        )

    def get_material_remain(self,
                            material: Product,
                            date: datetime) -> float:
        material_balance = self._get_material_balance(material, date)
        current_date = date
        latest_transaction_date = self.latest_transaction_date

        while current_date <= latest_transaction_date:
            material_balance = min(
                material_balance,
                self._get_material_balance(
                    material,
                    current_date
                )
            )
            current_date += _ONE_DAY_TIMEDELTA
        return material_balance

    def calc_manufacturing_date(self,
                                material: Product,
                                date: datetime,
                                quantity: float) -> datetime:
        result_date = date
        while self.get_material_remain(material, result_date) < quantity:
            result_date += _ONE_DAY_TIMEDELTA
        return result_date

    def _get_extremum_transaction_date(self,
                                       extremum_fn: callable) -> datetime:
        try:
            extremum_date = extremum_fn(map(
                attrgetter('date'),
                chain.from_iterable(self.transactions.values())
            ))
        except ValueError:
            extremum_date = datetime.now()
        return _round_up_to_day(extremum_date)

    @property
    def earliest_transaction_date(self) -> datetime:
        return self._get_extremum_transaction_date(min)

    @property
    def latest_transaction_date(self) -> datetime:
        return self._get_extremum_transaction_date(max)

    def is_enough_materials(self, entry, number_of_batches,
                            current_date) -> bool:

        if entry.amount >= \
                entry.product.batch_size * number_of_batches * 2:
            amount = entry.product.batch_size * number_of_batches
        else:
            amount = entry.amount

        if not self.is_enough_materials_for_product(
            entry.product.code,
            entry.order_name,
            entry.product,
            amount,
            current_date
        ):
            return False

        return True

    def is_enough_materials_for_product(self,
                                        initial_product_code: str,
                                        initial_order_name: str,
                                        product: Product, amount: float,
                                        current_date: datetime) -> bool:
        logger = self._logger
        report = True
        for child in product.product_entry:
            if child.product.id not in self.materials:
                if not self.is_enough_materials_for_product(
                    initial_product_code,
                    initial_order_name,
                    child.product,
                    amount * child.amount,
                    current_date
                ):
                    report = False
                continue
            if child.amount * amount > \
                    self.get_material_remain(child.product, current_date)\
                    and not child.product.product_entry:
                logger.debug(
                    'Изделие {} из заказа {} не будет добавлено в план '
                    'потому, что материала {} не хватает на складе'.format(
                        initial_product_code,
                        initial_order_name,
                        child.product.name
                    )
                )
                report = False
            if child.amount * amount > \
                    self.get_material_remain(child.product, current_date):
                if not self.is_enough_materials_for_product(
                    initial_product_code,
                    initial_order_name,
                    child.product,
                    amount * child.amount,
                    current_date
                ):
                    report = False

        return report

    def use_materials_for_order(self,
                                initial_product_code: str,
                                product: Product,
                                amount: float,
                                date: datetime) -> None:
        logger = self._logger

        if not product.product_entry:
            logger.debug(
                'На продукт {} списываем {} материала {}, '
                'на складе есть {}'.format(
                    initial_product_code,
                    amount,
                    product,
                    self.get_material_remain(product, date)
                )
            )
            self.transactions[product].append(
                StoreTransaction(
                    date - timedelta(seconds=1),
                    - amount,
                    {
                        'PRODUCT': initial_product_code
                    }

                )
            )
            return

        if amount <= self.get_material_remain(product, date):
            logger.debug(
                'На продукт {} списываем {} материала {}, '
                'на складе есть {}'.format(
                    initial_product_code,
                    amount,
                    product,
                    self.get_material_remain(product, date)
                )
            )
            self.transactions[product].append(
                StoreTransaction(
                    date - timedelta(seconds=1),
                    - amount,
                    {
                        'PRODUCT': initial_product_code
                    }
                )
            )
            return
        else:
            done = self.get_material_remain(product, date)
            logger.debug(
                'На продукт {} списываем {} материала {}, '
                'на складе есть {}'.format(
                    initial_product_code,
                    done,
                    product,
                    self.get_material_remain(product, date)
                )
            )
            self.transactions[product].append(
                StoreTransaction(
                    date - timedelta(seconds=1),
                    - done,
                    {
                        'PRODUCT': initial_product_code
                    }
                )
            )

        amount = amount - done

        for child in product.product_entry:

            if child.amount * amount <= \
                    self.get_material_remain(child.product, date):
                logger.debug(
                    'На продукт {} списываем {} материала {}, '
                    'на складе есть {}'.format(
                        initial_product_code,
                        child.amount * amount,
                        child.product,
                        self.get_material_remain(child.product, date)
                    )
                )
                self.transactions[child.product].append(
                    StoreTransaction(
                        date - timedelta(seconds=1),
                        - amount * child.amount,
                        {
                            'PRODUCT': initial_product_code
                        }
                    )
                )
                continue

            self.use_materials_for_order(
                initial_product_code,
                child.product,
                amount * child.amount,
                date
            )

    def column_file_report(self, config: dict) -> []:

        report = []

        all_dates = dates_list(
            from_date=self.earliest_transaction_date,
            to_date=self.latest_transaction_date,
            weekday=config.get('weekday'),
            monthday=config.get('monthday')
        )

        for material in self.transactions:

            row_balance = {
                'CODE': material.id,
                'NAME': material.name,
                'IDENTITY': material.code
            }

            row_plus = {
                'CODE': '(+) {}'.format(material.id),
                'NAME': material.name,
                'IDENTITY': material.code
            }

            row_minus = {
                'CODE': '(-) {}'.format(material.id),
                'NAME': material.name,
                'IDENTITY': material.code
            }

            for i, date in enumerate(all_dates):
                if i + 1 <= len(all_dates) - 1:
                    next_date = all_dates[i + 1]
                else:
                    next_date = datetime.max
                date_as_str = date.date()
                row_plus[date_as_str] = 0
                row_minus[date_as_str] = 0
                row_balance[date_as_str] = self._get_material_balance(
                    material,
                    _round_up_to_day(date)
                )
                for transaction in self.transactions[material]:
                    if not (date <= transaction.date < next_date):
                        continue
                    if transaction.quantity > 0:
                        row_plus[date_as_str] += transaction.quantity
                    if transaction.quantity < 0:
                        row_minus[date_as_str] += transaction.quantity

            report.append(row_balance)
            report.append(row_plus)
            report.append(row_minus)

        return report

    def column_file_bi_report(self, config: dict) -> []:

        report = []

        all_dates = dates_list(
            from_date=self.earliest_transaction_date,
            to_date=self.latest_transaction_date,
            weekday=config.get('weekday'),
            monthday=config.get('monthday')
        )

        report += [{
            'PARENT_LOCATION': 'Объекты',
            'LOCATION': '{}_{}'.format(material.name, material.code),
            'PARENT_METRIC': 'Показатели',
            'METRIC': 'Остаток',
            'PERIOD': date.date(),
            'VALUE': self._get_material_balance(
                material,
                _round_up_to_day(date)
            )
        } for date in all_dates
          for material in self.transactions]

        report += [{
            'PARENT_LOCATION': 'Объекты',
            'LOCATION': '{}_{}'.format(material.name, material.code),
            'PARENT_METRIC': 'Показатели',
            'METRIC': 'Приход НИ',
            'PERIOD': date.date(),
            'VALUE': self._get_material_cumulative_arrival_per_from(
                material,
                _round_up_to_day(date),
                'PURCHASE'
            )
        } for date in all_dates
          for material in self.transactions]

        report += [{
            'PARENT_LOCATION': 'Объекты',
            'LOCATION': '{}_{}'.format(material.name, material.code),
            'PARENT_METRIC': 'Показатели',
            'METRIC': 'Расход НИ',
            'PERIOD': date.date(),
            'VALUE': self._get_material_cumulative_usage(
                material,
                _round_up_to_day(date)
            )
        } for date in all_dates
          for material in self.transactions]

        report += [{
            'PARENT_LOCATION': 'Объекты',
            'LOCATION': '{}_{}'.format(material.name, material.code),
            'PARENT_METRIC': 'Расход НИ',
            'METRIC': product,
            'PERIOD': date.date(),
            'VALUE': self._get_material_cumulative_usage_per_product(
                material,
                _round_up_to_day(date),
                product
            )
        } for product in self._get_all_products_set
          for date in all_dates
          for material in self.transactions]

        report += [{
            'PARENT_LOCATION': 'Объекты',
            'LOCATION': '{}_{}'.format(material.name, material.code),
            'PARENT_METRIC': 'Расход НИ',
            'METRIC': 'НЗП',
            'PERIOD': date.date(),
            'VALUE': self._get_material_cumulative_usage_per_from(
                material,
                _round_up_to_day(date),
                'WIP'
            )
        } for date in all_dates
          for material in self.transactions]

        report += [{
            'PARENT_LOCATION': 'Объекты',
            'LOCATION': '{}_{}'.format(material.name, material.code),
            'PARENT_METRIC': 'Показатели',
            'METRIC': 'Исходный склад',
            'PERIOD': date.date(),
            'VALUE': self._get_material_cumulative_arrival_per_from(
                material,
                _round_up_to_day(date),
                'STORE'
            )
        } for date in all_dates
          for material in self.transactions]

        return report

    def read_store_from_column_file(self, config: dict,
                                    entities: Entities) -> None:
        logger = self._logger
        data = read_column_file(config)

        for row in data:
            material_id = row['CODE']

            if material_id is None:
                continue

            if material_id not in entities:
                continue

            if material_id not in self.materials:
                self.materials[material_id] = material = entities[material_id]
            else:
                material = self.materials[material_id]

            if row['QUANTITY'] > 0:
                self.transactions[material].append(
                    StoreTransaction(
                        datetime.strptime(row['DATE'], '%Y-%m-%d') - timedelta(1),
                        row['QUANTITY'],
                        {'FROM': row['FROM']}
                    )
                )

        for row in data:
            material_id = row['CODE']

            if material_id is None:
                continue

            if material_id not in entities:
                continue

            if material_id not in self.materials:
                self.materials[material_id] = material = entities[material_id]
            else:
                material = self.materials[material_id]

            if row['QUANTITY'] < 0:
                self.use_materials_for_order(
                    row['FROM'],
                    material,
                    -row['QUANTITY'],
                    datetime.strptime(row['DATE'], '%Y-%m-%d')
                )

